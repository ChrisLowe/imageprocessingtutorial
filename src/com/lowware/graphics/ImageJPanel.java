package com.lowware.graphics;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import java.io.File;
import java.io.IOException;
import javax.imageio.*;

public class ImageJPanel extends JPanel {

	public BufferedImage image;

	public int getWidth() {
		return image.getWidth();	
	}
	
	public int getHeight() {
		return image.getHeight();
	}
	
	public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);  //draws the image
    }

	public void loadImage(File imageFile) {
		try {
			image = ImageIO.read(imageFile);
		} catch (IOException ex) {
			String directory = new File(".").getAbsolutePath();
			System.err.println("Could not open " + imageFile.getAbsolutePath() + " at " + directory);
			System.exit(-1);
		}	
	}
	
	public void saveToFile(String filename) {
        try {
            // Save as PNG
            String fn = filename + ".png";
            File file = new File(fn);
            ImageIO.write(image, "png", file);
        } catch (IOException e) {}
    }


	public ImageJPanel(File imageFile) {
		loadImage(imageFile);
		image = Transformations.flipVertical(image);		
		String filename = imageFile.getAbsolutePath() + "6";
		saveToFile(filename);
	
	}
}


