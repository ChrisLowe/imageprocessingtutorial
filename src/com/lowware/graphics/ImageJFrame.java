package com.lowware.graphics;


import javax.swing.JFrame;
import java.io.File;


public class ImageJFrame extends JFrame
{
	public ImageJPanel panel;
	
	/**
	 *  Constructor
	 * 
	 *  Sets up a JFrame which contains an image
	 */
	public ImageJFrame(File imageFile) {
		super("Image processing frame");
		
		panel = new ImageJPanel(imageFile);  
		
		getContentPane().add(panel);
		setSize(panel.getWidth(), panel.getHeight());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}
}