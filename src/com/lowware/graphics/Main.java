package com.lowware.graphics;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;


public class Main
{
  	/**
	* Entry point
	*/
	public static void main(String[] args) {
	
		
		try {
			final String filename = args[0];
			final File imageFile = new File(filename);
	
			//Start the JFrame
			java.awt.EventQueue.invokeLater(new Runnable() {
				public void run() {
					new ImageJFrame(imageFile);
				}
			});

	
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("Usage: java -jar ImageProcessor.jar <filename>");
			System.exit(-1);
		}
	
	}
}